#!/bin/sh

echo "demo om multiline comments"

<<com
$0
$#
$*
$@
$?
$$
com

echo "var1.sh: $0"
echo "first parameter: $1"
echo "Second parameter: $2"
echo "Quoted values: $@"
echo " Quoted values: $*"
echo "No. of parametrs: $#"
